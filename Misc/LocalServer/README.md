# Pre-requisites

1.  Python 2.6 or Python3.3 or higher

# Setup Local Server

1.  Install Flask

    `$ pip install Flask`

2.  Run the http server

    `$ python index_http.py`

    Open up a web browser and type http://localhost:5000/test/

3.  Run the https server

    `$ python index_https.py`

    Open up a web browser and type http://localhost:5000/test/

# Setup self-signed Certificates

The certificates in the ssl/certificates folder were created with OpenSSL on a Mac. If these do not work then you can create your own certificates. Follow the instructions [here](https://support.rackspace.com/how-to/generate-a-csr-with-openssl/) for creating self-signed certificates with OpenSSL.
