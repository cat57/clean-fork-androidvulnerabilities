1. **OpenSocket-InformationLeak-Lean**
Apps that communicate with a remote server over an open port are vulnerable to information leak.

2. **UnEncryptedSocketComm-Lean**
Apps that communicate with a server over TCP/IP without encryption allow Man-in-the-Middle attackers to spoof server IPs by intercepting client-server data streams.
