1. **CheckValidity-InformationExposure-Lean**
Apps that do not check for the validity of a certificate when communicating with a server are
vulnerable to information exposure attacks.

2. **HttpConnection-MITM-Lean**
Apps that connect to a server via the *HTTP* protocol are vulnerable to Man-in-the-Middle (MITM) attacks.

3. **IncorrectHostNameVerification-MITM-Lean** Apps that do not check for the validity of a host name when communicating with a server are vulnerable to Man-in-the-Middle (MitM) attacks

4. **InvalidCertificateAuthority-MITM-Lean** Apps that do not check for the validity of a Certificate Authority when communicating with a server are vulnerable to MitM attacks

5. **JavaScriptExecution-CodeInjection-Lean** Apps that allow Javascript code to execute in a WebView may permit malicious code execution.

6. **UnpinnedCertificates-MITM-Lean** Apps that do not pin the certificates of servers they trust are vulnerable to MITM attacks.    

7. **UnsafeIntentURLImpl-InformationExposure-Lean** Apps that do not safely handle an incoming intent embedded inside a URI are vulnerable to information exposure via intent hijacking.

8. **WebView-NoUserPermission-InformationExposure** Apps that disclose sensitive information without explicitly requesting the user for permission are vulnerable to unintended information exposure.

9. **WebViewAllowFileAccess-UnauthorizedFileAccess-Lean** Apps that allow Javascript code to execute in a WebView without verifying where the JavaScript is coming from, can expose the app's resources

10. **WebViewIgnoreSSLWarning-MITM-Lean** Apps that ignore SSL errors when loading content in a WebView are vulnerable to MitM attacks

11. **WebViewInterceptRequest-MITM-Lean** Apps that do not validate resource request before loading them into a WebView are vulnerable to MitM attacks.

12. **WebViewOverrideUrl-MITM-Lean** Apps that do not validate page requests made from within a WebView, are vulnerable to MitM attcaks.
